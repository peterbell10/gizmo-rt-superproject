
MANITOU_DIR = $(CURDIR)/manitou
GIZMO_DIR = $(CURDIR)/gizmo-rt

CONFIG = $(CURDIR)/Config.sh
PERL = /usr/bin/perl
EXEC = $(CURDIR)/GIZMO

SUBMODULES = gizmo-rt manitou

.PHONY: GIZMO
GIZMO: $(MANITOU_DIR)/libManitou_CUDA/libManitou_CUDA.a
	$(MAKE) -C $(GIZMO_DIR) CONFIG=$(CONFIG) PERL=$(PERL) EXEC=$(EXEC) MANITOUDIR=$(MANITOU_DIR)

$(GIZMO_DIR)/config-makefile: gizmo-rt
	@:
$(GIZMO_DIR)/GIZMO_config.h: $(CONFIG) $(GIZMO_DIR)/config-makefile
	$(MAKE) -C $(GIZMO_DIR) -f $(GIZMO_DIR)/config-makefile CONFIG=$(CONFIG) PERL=$(PERL)

$(MANITOU_DIR)/libManitou_CUDA/libManitou_CUDA.a: manitou $(GIZMO_DIR)/GIZMO_config.h
	$(MAKE) -C $(MANITOU_DIR) GIZMO_DIR=$(GIZMO_DIR)

$(SUBMODULES): % : %/.git
$(addsuffix /.%, $(SUBMODULES)):
	git submodule update --init $(SUBMODULES)

.PHONY: clean
clean:
	-$(MAKE) -C $(GIZMO_DIR) clean
	-$(MAKE) -C $(MANITOU_DIR) clean
	-rm -f $(EXEC)

.PHONY: distclean
distclean:
	-$(MAKE) -C $(GIZMO_DIR) clean
	-$(MAKE) -C $(MANITOU_DIR) distclean
	-rm -f $(EXEC)

.PHONY: rebuild
rebuild: distclean
	$(MAKE) GIZMO
